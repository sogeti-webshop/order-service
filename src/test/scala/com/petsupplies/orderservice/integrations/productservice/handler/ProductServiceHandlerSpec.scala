package com.petsupplies.orderservice.integrations.productservice.handler

import cats._
import com.petsupplies.orderservice.integrations.productservice.ProductServiceM
import freestyle._
import freestyle.implicits._
import com.petsupplies.orderservice.integrations.productservice.handler.ProductServiceMOps._
import com.petsupplies.orderservice.integrations.productservice.handler.ProductServiceHandler._
import com.petsupplies.productservice.persistence.ProductRepository
import com.petsupplies.productservice.types.Product.{Product => DomainProduct}
import com.petsupplies.productservice.dsl.Product.getProduct
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, OneInstancePerTest}

class ProductServiceHandlerSpec extends FlatSpec with Matchers with OneInstancePerTest with MockFactory {
  trait Fixture {
    implicit val productRepository: ProductRepository = mock[ProductRepository]
  }

  "ProductServiceHandler" should "interpret interpret freestyle actions" in {
    new Fixture {
      val productFromRepository = DomainProduct(2, "product 2", "description 2", 19.99)

      (productRepository.getProduct _).expects(2).once().returning {
        fs2.Task.delay {
          Some(productFromRepository)
        }
      }

      val maybeProduct = ProductServiceM[ProductServiceM.Op].interpret(getProduct(2)).interpret[Id]

      maybeProduct match {
        case Some(product) => assert(product == productFromRepository)
        case None => fail()
      }
    }
  }
}
