package com.petsupplies.orderservice

import org.scalatest.{FlatSpec, Matchers}

class ConfigSpec extends FlatSpec with Matchers {
  "mongodatabase" should "have default connectionString" in {
    assert(Config.defaultMongoConnectionString === "mongodb://localhost:27017")
  }

  "App" should "have default port" in {
    assert(Config.defaultPort === 8082)
  }

  // The arguments to the functions of cors policies will be ignored and the actual values get returned
  "Cors filter" should "allow all origins" in {
    Config.corsPolicy.allowsOrigin("").get should be("*")
  }

  "Cors filter" should "allow only accept header" in {
    Config.corsPolicy.allowsHeaders(List("")).get should be(List("content-type", "accept"))
  }

  "Cors filter" should "accept GET, POST, PUT, DELETE" in {
    Config.corsPolicy.allowsMethods("").get should be(List("GET", "POST", "PUT", "DELETE"))
  }
}
