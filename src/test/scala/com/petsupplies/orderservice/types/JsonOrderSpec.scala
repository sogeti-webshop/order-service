package com.petsupplies.orderservice.types

import com.petsupplies.orderservice.types.JsonOrder._
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import org.scalatest.{FlatSpec, Matchers}

class JsonOrderSpec extends FlatSpec with Matchers {
  "JsonOrder" should "transforms of product id with quantities to list of ids" in {
    val jsonOrder = JsonOrder(
      List(ProductIdWithQuantity(1, 1), ProductIdWithQuantity(2, 1)),
      PersonalData("email", "asdaads", "dsasd", "2222 XL", "adsads")
    )

    assert(jsonOrder.productIds == List(1, 2))
  }

  "JsonOrder" should "transforms of product id with quantities to list of quantities" in {
    val jsonOrder = JsonOrder(
      List(ProductIdWithQuantity(1, 5), ProductIdWithQuantity(2, 4)),
      PersonalData("email", "asdaads", "dsasd", "2222 XL", "adsads")
    )

    assert(jsonOrder.quantities == List(5, 4))
  }
}
