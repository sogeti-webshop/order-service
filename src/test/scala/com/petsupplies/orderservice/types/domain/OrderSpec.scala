package com.petsupplies.orderservice.types.domain

import org.scalatest.{FlatSpec, Matchers}
import com.petsupplies.productservice.types.Product.{ Product => DomainProduct }
import com.petsupplies.orderservice.types.domain.ShoppingCart.OrderItem
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData

class OrderSpec extends FlatSpec with Matchers {
  "Order" should "be created from products" in {
    val order = Order.fromProducts(
      List(DomainProduct(1, "title", "desc", 12), DomainProduct(2, "title2", "desc2", 24)),
      List(2, 4),
      PersonalData("email", "asdaads", "dsasd", "2222 XL", "adsads")
    )

    assert(order.shoppingCart == List(OrderItem(DomainProduct(1, "title", "desc", 12), 2), OrderItem(DomainProduct(2, "title2", "desc2", 24), 4)))

    assert(order.orderDetails ==  PersonalData("email", "asdaads", "dsasd", "2222 XL", "adsads"))
  }
}
