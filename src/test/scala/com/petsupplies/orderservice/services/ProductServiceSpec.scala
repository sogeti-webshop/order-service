package com.petsupplies.orderservice.services

import com.petsupplies.productservice.persistence.ProductRepository
import com.petsupplies.orderservice.integrations.productservice.handler.ProductServiceHandler.freeStyleProductServiceHandler
import com.petsupplies.productservice.types.Product.{ Product => DomainProduct }
import com.petsupplies.productservice.types.Product.ProductId
import cats._
import freestyle._
import freestyle.implicits._
import fs2.Task
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, OneInstancePerTest}

class ProductServiceSpec extends FlatSpec with Matchers with OneInstancePerTest with MockFactory {
  trait Fixture {
    implicit val productRepository: ProductRepository = mock[ProductRepository]
    val productService = implicitly[ProductService[ProductService.Op]]
  }

  "ProductService" should "get all or none products" in {
    new Fixture {
      (productRepository.getProduct _).expects(2).once().returning {
        fs2.Task.delay {
          Some(DomainProduct(2, "product 2", "description 2", 19.99))
        }
      }

      (productRepository.getProduct _).expects(3).once().returning {
        fs2.Task.delay {
          Some(DomainProduct(3, "product 3", "description 3", 29.99))
        }
      }

      val maybeProducts: Option[List[Product]] = productService.getProducts(List(2, 3)).interpret[Id]

      assert(maybeProducts.isDefined)

      maybeProducts match {
        case Some(products) => assert(
          products == List(
            DomainProduct(2, "product 2", "description 2", 19.99),
            DomainProduct(3, "product 3", "description 3", 29.99)
          )
        )
        case None => fail()
      }
    }
  }

  // val productRepository = new ProductRepository {
  //   val products = List(
  //     DomainProduct(1, "product 1", "description 1", 9.99),
  //     DomainProduct(2, "product 2", "description 2", 19.99),
  //     DomainProduct(3, "product 3", "description 3", 29.99),
  //     DomainProduct(4, "product 4", "description 4", 39.99),
  //     DomainProduct(5, "product 5", "description 5", 49.99),
  //     DomainProduct(6, "product 6", "description 6", 59.99),
  //   )
  //   def getProduct(productId: ProductId): Task[Option[DomainProduct]] = fs2.Task.delay(
  //     Some()
  //   )

  //   def allProducts: Task[List[DomainProduct]] = ???
  // }
}
