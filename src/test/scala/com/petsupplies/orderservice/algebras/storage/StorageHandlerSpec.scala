package com.petsupplies.orderservice.algebras.storage

import freestyle._
import freestyle.implicits._
import cats._
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import com.petsupplies.orderservice.algebras.storage.StorageHandler._
import com.petsupplies.productservice.types.Product.{Product => DomainProduct}
import com.petsupplies.orderservice.types.domain.Order
import com.petsupplies.orderservice.types.domain.OrderDetails.OrderDetails
import com.petsupplies.orderservice.types.domain.ShoppingCart.{OrderItem, ShoppingCart}
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.{Completed, MongoCollection, MongoDatabase, Observable, SingleObservable}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, OneInstancePerTest}
import io.circe.generic.auto._
import io.circe.syntax._

class StorageHandlerSpec extends FlatSpec with OneInstancePerTest with Matchers with MockFactory {

  trait Fixture {
    implicit val mongoDatabase: MongoDatabase = mock[MongoDatabase]
    val storageHandler = implicitly[Storage[Storage.Op]]
  }

  "StorageHandler" should "persist order" in {
    new Fixture {
      val shoppingCart: ShoppingCart = List(
        OrderItem(DomainProduct(1, "product 1", "description 1", 9.99), 1),
        OrderItem(DomainProduct(2, "product 2", "description 2", 19.99), 3),
        OrderItem(DomainProduct(3, "product 3", "description 3", 29.99), 5)
      )

      val orderDetails: OrderDetails = PersonalData("email@address.nl", "Personal Data", "Turfschip 16", "1186 XL", "Zeddam")

      val order = Order(shoppingCart, orderDetails)

      val mongoCollection: MongoCollection[Document] = mock[MongoCollection[Document]]

        (mongoDatabase.getCollection[Document] _).expects("order").once().returning(mongoCollection)

      val document = Document.apply(order.asJson.toString)

      (mongoCollection.insertOne(_: Document)).expects(document).once().returning(
        SingleObservable.apply(Completed())
      )

      storageHandler.store(order).interpret[Id]
    }
  }
}

