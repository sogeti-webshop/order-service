package com.petsupplies.orderservice

import com.petsupplies.orderservice.algebras.storage.Storage
import com.petsupplies.orderservice.algebras.env.Env
import com.petsupplies.orderservice.algebras.logger.Logger
import com.petsupplies.orderservice.integrations.productservice.ProductServiceM
import com.petsupplies.orderservice.integrations.validation.Implicits.fsValidation
import freestyle._

@module trait App {
  val storage: Storage
  val logger: Logger
  val productService: ProductServiceM
  val env: Env
  val validation: fsValidation.ValidationM
}
