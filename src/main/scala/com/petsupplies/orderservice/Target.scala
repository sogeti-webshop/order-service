package com.petsupplies.orderservice

import com.twitter.util.Future

object Target {
  type Result[A] = Future[A]
}
