package com.petsupplies.orderservice.types

import com.petsupplies.orderservice.types.JsonOrder.ProductIdWithQuantity
import com.petsupplies.orderservice.types.domain.OrderDetails.OrderDetails
import com.petsupplies.orderservice.types.domain.ShoppingCart.{ProductId, Quantity}

case class JsonOrder(shoppingCart: List[ProductIdWithQuantity], orderDetails: OrderDetails) {
  def productIds: List[ProductId] = shoppingCart.map { _.productId }
  def quantities: List[Quantity] = shoppingCart.map { _.quantity }
}

object JsonOrder {
  case class ProductIdWithQuantity(productId: ProductId, quantity: Quantity)
}

