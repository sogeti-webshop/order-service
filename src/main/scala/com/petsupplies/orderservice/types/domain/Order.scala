package com.petsupplies.orderservice.types.domain

import com.petsupplies.orderservice.types.domain.OrderDetails.OrderDetails
import com.petsupplies.orderservice.types.domain.ShoppingCart.{OrderItem, ShoppingCart}
import com.petsupplies.productservice.types.Product.{ Product => DomainProduct }

case class Order(shoppingCart: ShoppingCart, orderDetails: OrderDetails)

object Order {
  def fromProducts(products: List[DomainProduct], quantities: List[Int], orderDetails: OrderDetails): Order = {
    val productsWithQuantities = products.zip(quantities)

    val shoppingCart = productsWithQuantities.map(x => OrderItem(x._1, x._2))

    Order(
      shoppingCart,
      orderDetails
    )
  }
}
