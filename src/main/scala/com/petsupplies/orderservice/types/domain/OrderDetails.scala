package com.petsupplies.orderservice.types.domain

import com.petsupplies.authenticationservice.types.PersonalData.PersonalData

object OrderDetails {
  type OrderDetails = PersonalData
}
