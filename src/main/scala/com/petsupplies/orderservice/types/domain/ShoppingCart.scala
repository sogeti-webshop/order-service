package com.petsupplies.orderservice.types.domain

import com.petsupplies.productservice.types.Product.{Product => DomainProduct}

object ShoppingCart {
  type ShoppingCart = List[OrderItem]

  case class OrderItem(product: DomainProduct, quantity: Quantity)

  type ProductId = Int
  type Quantity = Int
}

