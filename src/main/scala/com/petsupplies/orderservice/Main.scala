package com.petsupplies.orderservice

import cats.~>
import com.petsupplies.orderservice.api.Api
import com.petsupplies.orderservice.api.Api._
import com.petsupplies.orderservice.Config.{corsPolicy, defaultMongoConnectionString, defaultPort}
import com.twitter.finagle.{Http, ListeningServer}
import com.twitter.finagle.http.filter.Cors.HttpFilter
import com.twitter.util.{Await, Future}
import com.twitter.conversions.time._
import io.finch.Application
import freestyle._
import freestyle.implicits._
import freestyle.http.finch._
import freestyle.effects.validation._
import com.petsupplies.orderservice.algebras.env.Env
import com.petsupplies.productservice.persistence.ProductRepository._
import com.petsupplies.productservice.persistence.Transactor._
import com.petsupplies.productservice.persistence.ProductQuery._
import com.petsupplies.orderservice.algebras.storage.StorageHandler._
import com.petsupplies.orderservice.algebras.logger.LoggerHandler._
import com.petsupplies.orderservice.algebras.env.EnvHandler._
import com.petsupplies.orderservice.integrations.productservice.handler.ProductServiceHandler._
import com.petsupplies.orderservice.integrations.validation.Implicits.{fsValidation, futureMonadState}
import fsValidation.implicits._
import io.circe.generic.auto._
import io.finch.circe._
import org.mongodb.scala.{MongoClient, MongoDatabase}

object Main {

  def mongoDatabase[F[_]](
    implicit handler: F ~> Future,
    env: Env[F]
  ): FreeS[F, MongoDatabase] =
    for {
      connectionString <- env.get("PETSUPPLIES_ORDER_MONGODB_CONNECTION_STRING").map {
        _.getOrElse(defaultMongoConnectionString)
      }
    } yield MongoClient(connectionString).getDatabase("petsupplies-orders")

  def bootstrap[F[_]](
    implicit handler: F ~> Target.Result,
    api: Api[F],
    env: Env[F]
  ): FreeS[F, ListeningServer] =
    for {
      port <- env.get("PETSUPPLIES_ORDER_PORT").map(_.getOrElse(defaultPort))
    } yield {
      val httpFilter: HttpFilter = new HttpFilter(corsPolicy)

      Await.ready(Http.server.serve(":" + port, httpFilter andThen api.endpoints.toServiceAs[Application.Json]))
    }

  def main(args: Array[String]): Unit = {
    implicit val database: MongoDatabase = Await.result(mongoDatabase[Env.Op].interpret[Future], 10.seconds)

    Await.result(bootstrap[App.Op].interpret[Target.Result])
  }
}
