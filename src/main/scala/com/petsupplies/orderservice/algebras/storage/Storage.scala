package com.petsupplies.orderservice.algebras.storage

import com.petsupplies.orderservice.types.domain.Order
import freestyle.free

@free sealed trait Storage {
  def store(order: Order): FS[Unit]
}
