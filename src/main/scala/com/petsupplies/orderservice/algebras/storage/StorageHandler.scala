package com.petsupplies.orderservice.algebras.storage

import cats.Monad
import com.petsupplies.orderservice.types.domain.Order
import io.circe.generic.auto._
import io.circe.syntax._
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.{Completed, MongoCollection, MongoDatabase}

object StorageHandler {
  implicit def storageHandler[M[_]: Monad](
    implicit mongoDatabase: MongoDatabase
  ): Storage.Handler[M] = new Storage.Handler[M] {
    override def store(order: Order): M[Unit] = Monad[M].pure {
      val collection: MongoCollection[Document] = mongoDatabase.getCollection("order")

      val document: Document = Document(order.asJson.toString)

      collection.insertOne(document).subscribe((x: Completed) => ())
    }
  }
}
