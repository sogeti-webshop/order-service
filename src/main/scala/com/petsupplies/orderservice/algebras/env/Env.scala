package com.petsupplies.orderservice.algebras.env

import freestyle.free

@free sealed trait Env {
  def get(key: String): FS[Option[String]]
}
