package com.petsupplies.orderservice.algebras.env

import cats.Monad
import com.twitter.util.Future

object EnvHandler {
  implicit def envHandler[M[_]: Monad]: Env.Handler[M] = new Env.Handler[M] {
    def get(key: String): M[Option[String]] = Monad[M].pure(sys.env.get(key))
  }
}
