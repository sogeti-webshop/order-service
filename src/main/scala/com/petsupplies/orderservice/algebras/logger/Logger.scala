package com.petsupplies.orderservice.algebras.logger

import freestyle.free

@free sealed trait Logger {
  def error(message: String): FS[Unit]
  def warning(message: String): FS[Unit]
  def info(message: String): FS[Unit]
  def debug(message: String): FS[Unit]
}
