package com.petsupplies.orderservice.algebras.logger

import cats.Monad

object LoggerHandler {
  implicit def loggerHandler[M[_]: Monad]: Logger.Handler[M] = new Logger.Handler[M] {
    def error(message: String): M[Unit] = Monad[M].pure(
      println(s"Error: $message")
    )

    def warning(message: String): M[Unit] = Monad[M].pure(
      println(s"Warning: $message")
    )

    def info(message: String): M[Unit] = Monad[M].pure(
      println(s"Info: $message")
    )

    def debug(message: String): M[Unit] = Monad[M].pure(
      println(s"Debug: $message")
    )
  }
}
