package com.petsupplies.orderservice.services

import com.petsupplies.orderservice.algebras.storage.Storage
import com.petsupplies.orderservice.algebras.logger.Logger
import com.petsupplies.orderservice.types.domain.Order
import freestyle._

@module
trait OrderService {
  val storage: Storage
  val logger: Logger

  def store(order: Order): FS.Seq[Unit] =
    for {
      _ <- logger.info("Storing an order")
      _ <- storage.store(order)
      _ <- logger.info("Order stored")
    } yield ()
}
