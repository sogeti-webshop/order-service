package com.petsupplies.orderservice.services

import cats.Applicative
import com.petsupplies.orderservice.integrations.productservice.ProductServiceM
import com.petsupplies.orderservice.types.domain.ShoppingCart.ProductId
import com.petsupplies.productservice.types.Product.Product
import freestyle.module
import cats.instances.option._
import cats.instances.list._

@module
trait ProductService {
  val productService: ProductServiceM

  def getProducts(productIds: List[ProductId]): FS[Option[List[Product]]] =
    for {
      listOfProduct <- productService.getProducts(productIds)
    } yield
      Applicative[Option]
          .sequence(listOfProduct)
}
