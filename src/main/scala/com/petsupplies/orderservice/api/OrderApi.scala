package com.petsupplies.orderservice.api

import cats.data.Validated.{Invalid, Valid}
import cats.{Id, ~>}
import com.petsupplies.orderservice.services.{OrderService, ProductService}
import com.petsupplies.orderservice.types.JsonOrder
import com.twitter.util.Future
import io.finch.{BadRequest, Endpoint, Ok, Output, jsonBody, post}
import io.finch.circe._
import freestyle._
import freestyle.implicits._
import freestyle.http.finch._
import io.circe.generic.auto._
import com.petsupplies.authenticationservice.types.PersonalData
import com.petsupplies.authenticationservice.util.validation.ValidationFailed
import com.petsupplies.orderservice.integrations.validation.Implicits.fsValidation
import com.petsupplies.orderservice.types.domain.Order
import com.petsupplies.authenticationservice.util.validation.semigroup.ValidationErrorSemigroup._

class OrderApi[F[_]](
  implicit orderService: OrderService[F],
  productService: ProductService[F],
  validation: fsValidation.ValidationM[F],
  handler: F ~> Future
) {
  private val resourceName = "order"

  val placeOrder: Endpoint[Unit] = post(resourceName :: jsonBody[JsonOrder]) {
    jsonOrder: JsonOrder => {
      val valildatePersonalData = PersonalData.validatePersonalData(jsonOrder.orderDetails)

      val program = validation.fromValidatedNel(valildatePersonalData)
          .flatMap[Output[Unit]] {
        case Valid(orderDetails) =>
          for {
            maybeStored <- productService.getProducts(jsonOrder.productIds)
                .flatMap { // for some reason, pattern match is not working in for comprehension. Thats why this flatMap is done inline
                  case Some(products) => {
                    val order = Order.fromProducts(products, jsonOrder.quantities, orderDetails)

                    orderService.store(order).map(_ => Option(()))
                  }
                  case None => FreeS.pure(Option.empty[Unit])
                }
          } yield maybeStored.fold[Output[Unit]](BadRequest(new Exception("Not all products are available in the webshop or cannot be ordered")))(Ok(_))

        case Invalid(errors) =>
          FreeS.pure {
            BadRequest(
              new Exception(
                errors.reduce match {
                  case ValidationFailed(message) => message
                }
              )
            )
          }
      }

      program
    }
  }

  val endpoints: Endpoint[Unit] = placeOrder
}

object OrderApi {
  implicit def instance[F[_]](
    implicit orderService: OrderService[F],
    productService: ProductService[F],
    validation: fsValidation.ValidationM[F],
    handler: F ~> Future
  ): OrderApi[F] = new OrderApi[F]
}
