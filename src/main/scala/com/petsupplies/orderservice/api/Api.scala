package com.petsupplies.orderservice.api

class Api[F[_]](
  implicit orderApi: OrderApi[F]
) {
  val endpoints = orderApi.endpoints
}

object Api {
  implicit def instance[F[_]](
    implicit orderApi: OrderApi[F]
  ): Api[F] = new Api[F]
}

