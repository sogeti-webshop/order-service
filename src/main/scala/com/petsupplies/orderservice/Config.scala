package com.petsupplies.orderservice

import com.twitter.finagle.http.filter.Cors.Policy

object Config {
  val defaultMongoConnectionString: String = "mongodb://localhost:27017"
  val defaultPort: Int = 8082

  val corsPolicy: Policy = Policy(
    allowsOrigin = _ => Some("*"),
    allowsMethods = _ => Some(Seq("GET", "POST", "PUT", "DELETE")),
    allowsHeaders = _ => Some(Seq("content-type", "accept"))
  )
}
