package com.petsupplies.orderservice.integrations.validation

import cats.MonadState
import com.petsupplies.authenticationservice.util.validation.ValidationError
import com.twitter.util.Future
import freestyle.effects.validation
import freestyle.effects.validation.ValidationProvider

object Implicits {
  val fsValidation = validation[ValidationError]

  // this typeclass is not implementable, but necessary for the freestyle validation module.
  implicit val futureMonadState: MonadState[Future, List[ValidationError]] = new MonadState[Future, List[ValidationError]] {
    def get: Future[List[ValidationError]] = Future.value(List.empty[ValidationError])

    def set(s: List[ValidationError]): Future[Unit] = Future {
    }

    def pure[A](x: A): Future[A] = Future.value(x)

    def flatMap[A, B](fa: Future[A])(f: (A) => Future[B]): Future[B] = fa flatMap f

    def tailRecM[A, B](a: A)(f: (A) => Future[Either[A, B]]): Future[B] =
      io.catbird.util.twitterFutureInstance.tailRecM(a)(f)
  }
}


