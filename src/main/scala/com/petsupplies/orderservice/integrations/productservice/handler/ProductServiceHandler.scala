package com.petsupplies.orderservice.integrations.productservice.handler

import cats.Monad
import com.petsupplies.orderservice.integrations.productservice.ProductServiceM
import com.petsupplies.productservice.dsl.Product.ProductF
import cats.instances.future._
import com.petsupplies.productservice.persistence.ProductRepository
import com.petsupplies.productservice.interpreter.ProductInterpreter
import com.petsupplies.productservice.ProductService
import com.petsupplies.productservice.types.Product.ProductId
import ProductServiceMOps._
import freestyle.{FreeS, FreeSLift}

object ProductServiceHandler {
  implicit def freeStyleProductServiceHandler[M[_] : Monad](
    implicit productRepository: ProductRepository
  ): ProductServiceM.Handler[M] = new ProductServiceM.Handler[M] {
    def interpret[A](f: ProductF[A]) =
      f.foldMap(ProductInterpreter.interpret[M])

    def getProducts(productIds: List[ProductId]) =
      interpret(ProductService.getProducts(productIds))
  }

  implicit def freeSLiftProductServiceM[F[_] : ProductServiceM]: FreeSLift[F, ProductF] =
    new FreeSLift[F, ProductF] {
      def liftFSPar[A](productF: ProductF[A]): FreeS.Par[F, A] = ProductServiceM[F].interpret(productF)
    }
}
