package com.petsupplies.orderservice.integrations.productservice.handler

import com.petsupplies.orderservice.integrations.productservice.ProductServiceM
import com.petsupplies.orderservice.integrations.productservice.handler.ProductServiceHandler.freeStyleProductServiceHandler
import com.petsupplies.productservice.dsl.Product.ProductF

object ProductServiceMOps {
  implicit class ProductServiceMOps[A](productF: ProductF[A]) {
    def interpret[F[_]](implicit productService: ProductServiceM[F]) =
      productService.interpret(productF)
  }
}
