package com.petsupplies.orderservice.integrations.productservice

import com.petsupplies.productservice.dsl.Product.ProductF
import com.petsupplies.productservice.types.Product.Product
import com.petsupplies.productservice.types.Product.ProductId
import freestyle.free

@free sealed trait ProductServiceM {
  def interpret[A](f: ProductF[A]): FS[A]
  def getProducts(products: List[ProductId]): FS[List[Option[Product]]]
}


