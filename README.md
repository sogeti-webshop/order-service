# OrderService

This service does everything order related.

## Development

Make sure Scala V2.12.2 and SBT 0.13.6 are installed. The following commands can be used.

To run unit tests:
```
sbt test
```

To compile the application:
```
sbt compile
```

To run the application:
```
sbt run
```

Whenever the application is used, there needs to be a mongodb instance which can be changed by setting an environment variable. For development, `docker-compose up -d` can be ran to setup a mongodb instance that can be accessed from the application.

The application allows change of settings by setting environment variables. The following environment variables will be recognized by the application.

```
PETSUPPLIES_ORDER_MONGODB_CONNECTION_STRING: this one represents the mongodb connection string and will default to "mongodb://localhost:27017"
PETSUPPLIES_ORDER_PORT: The port where the application will run on the host and will default to "8082"
```

## Sonar

To verify testcoverage, you can publish to sonar by running the `publish-to-sonar.sh` script.

## CI/CD

Since there is no access to servers, the CI is simulated with a shellscript that will dockerize the application whenever triggered (manually for now).

```
sh ./build-and-publish.sh
```

This will run tests, compiles the code and assembles all it's dependencies. Next up it will create a new docker image and publish this to the registry.

