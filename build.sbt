name := "OrderService"

version := "0.1.0"

scalaVersion := "2.12.2"

val finchVersion = "0.15.1"
val circeVersion = "0.8.0"

// Dependencies
libraryDependencies ++= Seq(
  "com.github.finagle" %% "finch-core" % "0.14.1",
  "com.github.finagle" %% "finch-circe" % "0.14.1",
  "io.circe" %% "circe-generic" % circeVersion,
  "io.frees" %% "freestyle" % "0.3.1",
  "io.frees" %% "freestyle-http-finch" % "0.3.1",
  "io.frees" %% "freestyle-effects" % "0.3.1",
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.1.0",
  "com.petsupplies" %% "productservice" % "0.2.0",
  "com.petsupplies" %% "authenticationservice" % "0.2.0",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % "test"
)
coverageExcludedFiles := ".*macro.*;.*integrations.validation.*"
// Compiler plugins
addCompilerPlugin("org.scalameta" % "paradise" % "3.0.0-M9" cross CrossVersion.full)

// Sonar properties to push coverage and style report
sonarProperties ++= Map(
  "sonar.host.url" -> "http://localhost:9000",
  "sonar.scoverage.reportPath" -> "target/scala-2.12/scoverage-report/scoverage.xml"
)

// Assemble settings to publish fat jar with deps
assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}

lazy val commonSettings = Seq(
  version := "0.1.0",
  organization := "com.petsupplies",
  scalaVersion := "2.12.2",
  test in assembly := {}
)

lazy val orderService = (project in file(".")).
  settings(commonSettings: _*)

// Neccessary to publish the assembled jar to the target repository
artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)

enablePlugins(SonarRunnerPlugin)
