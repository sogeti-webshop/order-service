FROM debian:latest

RUN apt-get --yes update && apt-get --yes upgrade && apt-get --yes install default-jre

COPY ./target/scala-2.12/OrderService-assembly-0.1.0.jar /opt/

CMD ["java", "-jar", "/opt/OrderService-assembly-0.1.0.jar"]
